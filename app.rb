require './conf'
##################
### Les routes ###
##################
get '/' do
erb :index
end

get '/log' do
	@logs = Post.all
	erb :'log/index'
end

get '/log/new' do
	erb :'log/new'
end

##################
###  SYS  LOG  ###
##################
get '/log/:id' do |id|
	@logs = Post.get!(id)
	erb :'log/del'
end

get '/log/:id/edit' do |id|
	@logs = Post.get!(id)
	erb :'log/edit'
end

##################
###  SYS POST  ###
##################
#### Cree Sont Frist Log ####
post '/log' do
	log = Post.new(params[:message])
	if log.save
		redirect '/log'
	else
		redirect '/log/new'
	end
end
#### Modifier L'un des logs####
put '/log/:id' do |id|
	log = Post.get!(id)
	success = log.update!(params[:message])

	if success
		redirect "/log/#{id}"
	else
		redirect "/log/#{id}/edit"
	end
end
#### Supprimier L'un des logs####
delete '/log/:id' do |id|
	log = Post.get!(id)
	log.destroy!
	redirect "/log"
end